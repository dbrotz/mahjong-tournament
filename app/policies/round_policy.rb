class RoundPolicy < ApplicationPolicy
  def initialize(user, round)
    @user = user
	@round = round
  end

  def new?
    @user.present?
  end

  def create?
    @round.tournament.user == @user
  end

  def edit?
    @round.tournament.user == @user
  end

  def update?
    @round.tournament.user == @user
  end
end
