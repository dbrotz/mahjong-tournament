class TournamentPolicy < ApplicationPolicy
  def initialize(user, tournament)
    @user = user
	@tournament = tournament
  end

  def edit?
    @tournament.user == @user
  end
end
