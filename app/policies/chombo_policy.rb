class ChomboPolicy < ApplicationPolicy
  def initialize(user, chombo)
    @user = user
	@chombo = chombo
  end

  def create?
    @chombo.player.tournament.user == @user
  end
end

