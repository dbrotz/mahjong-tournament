class RoundController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit]

  def new
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
    @round = Round.new
	#@player_list = @tournament.pick_matches
  end

  def edit
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
    @round = @tournament.rounds.find_by(id: params[:id])

    authorize @round
  end

  def update
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
    @round = @tournament.rounds.find_by(id: params[:id])
    authorize @round
    
    params[:players].each_with_index do |v, i|
      player_round = PlayerRound.find_by(round: @round, player_id: @tournament.players.find_by_name(v))
      player_round.update!(score: params[:start_score][i])
    end
    @round.score_oka
    redirect_to tournament_round_index_url(@tournament.alpha_id)
  end

  def show
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
    @rounds = @tournament.rounds.where(round_number: (params[:id].to_i)).order(order: :asc)
  end

  def index
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
    @rounds = @tournament.rounds.order(round_number: :asc, order: :asc)
  end

  def create
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
    curr_round = @tournament.current_round_number
    authorize(@tournament, :edit?)

    #A game hasn't been finished/completed.
    # @tournament.player_rounds.count > 0 && 
    if @tournament.player_rounds.where(round_id: @tournament.rounds.where(round_number: curr_round).map(&:round_number)).where(score: nil, oka: nil).count != 0
      flash[:error] = "Current game has not been completed."
      redirect_to tournament_url(@tournament.alpha_id)
      return
    end

    i = 0
    until i > @tournament.max_games do
      if params[:players][i].present? && params[:players][i + 1].present? && params[:players][i + 2].present? && params[:players][i + 3].present?
        @round = Round.create!(round_number: curr_round + 1, order: i, tournament: @tournament,
          player_rounds: [
            PlayerRound.new(tournament: @tournament, player: @tournament.players.find_by_name(params[:players][i    ]), wind: 0),
            PlayerRound.new(tournament: @tournament, player: @tournament.players.find_by_name(params[:players][i + 1]), wind: 1),
            PlayerRound.new(tournament: @tournament, player: @tournament.players.find_by_name(params[:players][i + 2]), wind: 2),
            PlayerRound.new(tournament: @tournament, player: @tournament.players.find_by_name(params[:players][i + 3]), wind: 3)
            ])
      end
      i += 4
    end
    if @round.invalid?
      render :new
      return
    end
    redirect_to tournament_url(@tournament.alpha_id)
  end

  def round_params
    params.require(:players)
  end
end
