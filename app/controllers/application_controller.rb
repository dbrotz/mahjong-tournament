class ApplicationController < ActionController::Base
  include Pundit

  helper_method :authorized?

  def index
    @errors = nil
  end

  def search
    @tournament = Tournament.find_by_alpha_id(params[:search].upcase)

    if @tournament.nil?
      #redirect_to root_url, flash: {error: "Tournament does not exist."}
      @errors = "Tournament #{params[:search]} does not exist"
      render :index
      return
    end
    redirect_to tournament_url(@tournament.alpha_id)
  end
  def authorized? object, params = nil
    authorize(object, params)
    return true
  rescue Pundit::NotAuthorizedError => e
    return false
  end

  protected
  def set_tournament
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
  end
end
