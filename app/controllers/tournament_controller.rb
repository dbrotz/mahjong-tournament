class TournamentController < ApplicationController
  before_action :set_tournament, except: [:show, :create]

  def show
    @tournament = Tournament.find_by_alpha_id(params[:id])
  end

  def countdown
  end

  def rankings
  end

  def help
  end

  def schedule
  end

  def new
    @tournament = Tournament.new
    @participants = ""
  end

  def start_countdown
    authorize @tournament, :edit?
    @tournament.update!(countdown: DateTime.now.utc)
    redirect_to tournament_countdown_url(@tournament.alpha_id)
  end

  def create
    @participants = params[:participants]
    @user = User.new
    @tournament = Tournament.new(tournament_params.merge(players: @participants.split("\r\n").map do |player| Player.new(tournament: @tournament, name: player) end, user: @user))

    if @tournament.invalid?
      render :new
      return
    end

    @tournament.save
    @user.email = "#{@tournament.alpha_id}@test.com"
    @user.save
    sign_in(@user)
    redirect_to tournament_url(@tournament.alpha_id)
  end

  def tournament_params
    params.require(:tournament).permit(
      :name, :total_rounds, :time_limit, :start_score, :oka_first, :oka_second, :schedule
    )
  end
end
