class PlayerController < ApplicationController
  before_action :set_tournament, only: [:create]

  def new
    @player = Player.new
  end

  def create
    @player.new(player_params.merge(tournament: @tournament))

    if @player.save
      redirect to player_success_url
    else
      render :new
    end
  end
  
  def success

  end

  def player_params
    params.require(:tournament).permit(
      :name
    )
  end
end

