class ChomboController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create]

  def new
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
    @chombo = Chombo.new
  end

  def create
    @tournament = Tournament.find_by_alpha_id(params[:tournament_id])
    @chombo = Chombo.new(player_id: @tournament.players.find_by_name(params[:chombo][:player_name]).try(:id))

    authorize @chombo
	@chombo.save
    redirect_to tournament_url(@tournament.alpha_id)
  end
end
