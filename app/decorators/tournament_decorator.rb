class TournamentDecorator < ApplicationDecorator
  delegate_all

  def player_list
    options = ""
    players.sort_by do |player|
	  player.name
	end.each do |player|
      options += "<option value='#{player.name}'></option>"
	end
	return options.html_safe
  end
end
