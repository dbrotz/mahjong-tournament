class Tournament < ApplicationRecord
  has_many :rounds, dependent: :destroy
  has_many :players, dependent: :destroy
  has_many :player_rounds, dependent: :destroy

  belongs_to :user

  validate :oka_compare
  validate :validate_players
  validates_presence_of :name
  validates :total_rounds, numericality: { greater_than_or_equal_to:  0 }
  validates :time_limit, numericality: { greater_than_or_equal_to:  0 }
  validates :start_score, numericality: { greater_than_or_equal_to:  0 }
  validates :oka_first, numericality: { greater_than:  0 }
  validates :oka_second, numericality: { greater_than:  0 }

  before_create :setup

  def initialize(args = {})
    super(args)
  end

  def setup
    value = ""; 5.times{value << (65 + rand(25)).chr}
    self.alpha_id = value
  end

  def oka_third
    -self.oka_second
  end

  def oka_fourth
    -self.oka_first
  end

  def current_round
    self.rounds.select(:round_number).order(round_number: :desc).first
  end

  def current_round_number
    self.rounds.order(round_number: :desc).max.try(:round_number) || 0
  end

  def schedule=(value)
    write_attribute(:schedule, nil) if value.nil?
    write_attribute(:schedule, ActionView::Base.full_sanitizer.sanitize(value).gsub(/\R+/, "<br>"))
  end

  def max_games
    players.count / 4
  end

  def players_rank
    #ActiveRecord::Base.connection.exec_query("SELECT player_id, SUM(oka) as sum_oka FROM player_rounds WHERE player_rounds.tournament_id = #{id} GROUP BY player_id ORDER BY sum_oka DESC").rows
    rows = ActiveRecord::Base.connection.exec_query("
      SELECT player_rounds.player_id, SUM(oka) - ((SELECT COUNT(*) FROM chombos ch WHERE player_rounds.player_id = ch.player_id) * #{self.chombo_cost}) as sum_oka 
        FROM player_rounds WHERE player_rounds.tournament_id = #{self.id}
        GROUP BY player_rounds.player_id 
        ORDER BY sum_oka DESC").rows
    rows = ActiveRecord::Base.connection.exec_query("
      SELECT players.id, 0
        FROM players WHERE players.tournament_id = #{self.id}
        ORDER BY players.id ASC").rows if rows.empty?
    return rows
  end

  def oka_compare
    if oka_first < oka_second
      errors.add(:base, "Oka first must be greater than oka second.")
    end
  end

  def validate_players
    errors.add(:players, "Not enough players") if players.length < 4
  end

  #For every player figure out how many matches they have played with every player
  #Then sort this list and pick max_games leaders. These leaders will then one at
  #a time pick the player from their generated list that has played the least
  #amount of games with the leader.
  def pick_matches
    matches = Struct.new(:player_lookup, :played_table, :leaders).new({ }, Array.new(players.count), [])
    sorted_arr = []
    # An array of hash tables each element of played_table represents the nth player found in the tournaments.player array.
    # Each table's key is the player's id, and the value is how many times that player has been played the person 
    # represented by the array.

    matches.played_table.each_with_index do |item, index|
      matches.played_table[index] = { }
	  players.each do |p|
        matches.played_table[index][p.id] = 0
      end
    end
	player_ids = players.map(&:id)
	player_idx = {}
	players.each_with_index do |item, index|
      player_idx[item.id] = index
	end
    matches.leaders << player_ids.sample
     (max_games - 1).times do |i|
      begin
        rand_number = player_ids.sample
        matches.leaders << rand_number unless matches.leaders.include?(rand_number)
      end while matches.leaders.count <= (i + 1)
    end

    players.each_with_index do |p, i|
      matches.player_lookup[p.id] = i
    end
    players.each_with_index do |p, i|
      played = player_rounds.select(:player_id).where(round_id: PlayerRound.select(:round_id).where(player: p)).where.not(player: p).group(:player_id).map(&:player_id)
      played.each do |o|
        #matches.played_table[i][matches.player_lookup[o]] = matches.played_table[i][matches.player_lookup[o]] + 1
        matches.played_table[i][o] = matches.played_table[i][o] + 1
      end
    end
    matches.played_table.each do |a|
      sorted_arr << a.sort_by(&:last) 
    end
    #Done initializing data.

    pick_list = matches.leaders.dup #Players who have already been picked.
    games = Array.new(max_games)
    games.each_with_index do |item, index|
      games[index] = Array.new(4)
      games[index][0] = matches.leaders[index]
    end

    for i in 1..3
      matches.leaders.each_with_index do |picker, index|
		picker = player_idx[picker]
        begin
          #keep picking until we find a non-picked player.
          games[index][i] = sorted_arr[picker].shift.first
        end while pick_list.include?(games[index][i])
        pick_list << games[index][i]
        new_scores = score_players(matches, games[index], matches.player_lookup[sorted_arr[picker].shift.first])
        #Resort the sorted_arr adding penalties to players who haven't been chosen but have already played chosen players.
        sorted_arr[picker].map.with_index.sort_by do |item, index|
          new_scores[index]
        end
      end
    end
    return games.map do |array|
      array.map do |item|
        Player.find(item)
      end
    end
  end

  def score_players matches, game, player_index
    scores = Array.new(matches.played_table[player_index].size).fill(0)

    matches.played_table[player_index].each do |key, val|
      index = matches.player_lookup[key]
      game.each do |item|
        next if matches.played_table[index][item].nil?
        scores[index] += matches.played_table[index][item]
      end
    end
    return scores
  end
end
