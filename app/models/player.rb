class Player < ApplicationRecord
  belongs_to :tournament
  has_many :chombos, dependent: :destroy
  has_many :player_rounds, dependent: :destroy
  validate :unique_name

  def unique_name
    errors.add(:name, "A player with this name already exists in the tournament.") if tournament.players.where(name: self.name).count > 0
  end
end
