class PlayerRound < ApplicationRecord
  belongs_to :tournament
  belongs_to :round
  belongs_to :player

  validate :validate_player
  validate :validate_score

  enum wind: {
    east: 0,
    south: 1,
    west: 2,
    north: 3
  }

  def validate_player
    errors.add(:player, "#{player.name} is already is playing in a round.") if tournament.player_rounds.joins(:round).where(rounds: { round_number: self.round.round_number }, player: player).count > 1
  end

  def validate_score
    errors.add(:score, "Sum of scores is greater than max allowed") if self.round.player_rounds.sum(:score) > self.tournament.start_score * 4
  end
end
