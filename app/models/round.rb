class Round < ApplicationRecord
  belongs_to :tournament
  has_many :player_rounds, dependent: :destroy

  def score_oka
    players = player_rounds.order(score: :desc, wind: :asc)
    uma = [tournament.oka_first * 1000, tournament.oka_second * 1000, tournament.oka_third * 1000, tournament.oka_fourth * 1000]
    diff = (30000 - tournament.start_score) * 4

    player_rounds.find(players[0].id).update(oka: ((players[0].score.round(-3) - 30000 + uma.first + diff )   / 1000))
    player_rounds.find(players[1].id).update(oka: ((players[1].score.round(-3) - 30000 + uma.second)          / 1000))
    player_rounds.find(players[2].id).update(oka: ((players[2].score.round(-3) - 30000 + uma.third)           / 1000))
    player_rounds.find(players[3].id).update(oka: ((players[3].score.round(-3) - 30000 + uma.fourth)          / 1000))
  end

end
