Rails.application.routes.draw do
  devise_for :users
  root 'application#index'
  post '/', to: 'application#search'

  resources :tournament, only: [:new, :create, :show] do
    get 'rankings'
    get 'countdown'
	get 'help'
    get 'schedule'
    post 'start_countdown'
	resources :round
	resources :player, only: [:new, :create] do
      get 'success'
	end
	resources :chombo, only: [:new, :create]
  end
end
