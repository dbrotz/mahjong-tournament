# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

#shared_path = "#{fetch(:deploy_to)}/shared"
#current_path = "#{fetch(:deploy_to)}/current"

set :application, "mahjong-tournament"
set :repo_url, "git@gitlab.com:Raugharr/mahjong-tournament.git"
set :user, "deploy"
set :stages, %w(production)
set :migration_role, :db
set :rbenv_ruby, '2.5.0'
set :deploy_to, "/home/deploy/mahjong-tournament"
set :deploy_via, :copy
set :ssh_options, {
  user: "deploy", # overrides user setting above
  keys: %w(/home/deploy/.ssh/id_rsa),
  forward_agent: true,
  auth_methods: %w(publickey)
}
append :linked_files, "config/master.key"
append :linked_files, "config/puma.rb"

set :puma_threads,    [4, 16]
set :puma_workers,    0
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_conf, "#{shared_path}/config/puma.rb"

set :enable_ssl,      true

set :nginx_ssl_certificate, '/etc/letsencrypt/live/riichitournaments.com/fullchain.pem'
set :nginx_ssl_key, '/etc/letsencrypt/live/riichitournaments.com/privkey.pem'
set :nginx_use_ssl, true
set :nginx_server_name, "localhost riichitournaments.com www.riichitournaments.com"

set :tests, ["spec"]

namespace :deploy do
  desc "Runs test before deploying, can't deploy unless they pass"
  task :run_tests do
    run_locally do
      test_log = "log/capistrano.test.log"
      tests = fetch(:tests)
      tests.each do |test|
        info "--> Running tests locally: '#{test}', please wait ..."
        unless test(:rspec, "--tag ~js #{test}  > #{test_log} 2>&1")
          warn "--> Tests: '#{test}' failed. Results in: #{test_log} and below:"
          execute :cat, test_log
          exit;
        end
        info "--> '#{test}' passed"
      end
      info "--> All tests passed"
      execute :rm, test_log
    end
  end
  before :deploy, "deploy:run_tests"
end

#namespace :puma do
#  desc 'Create Directories for Puma Pids and Socket'
#  task :make_dirs do
#    on roles(:app) do
#      execute "mkdir #{shared_path}/tmp/sockets -p"
#      execute "mkdir #{shared_path}/tmp/pids -p"
#    end
#  end
#
#  before :start, :make_dirs
#end
#
#namespace :deploy do
#  desc "Make sure local git is in sync with remote."
#  task :check_revision do
#    on roles(:app) do
#      unless `git rev-parse HEAD` == `git rev-parse origin/master`
#        puts "WARNING: HEAD is not the same as origin/master"
#        puts "Run `git push` to sync changes."
#        exit
#      end
#    end
#  end
#
#  desc 'Initial Deploy'
#  task :initial do
#    on roles(:app) do
#      before 'deploy:restart', 'puma:start'
#      invoke 'deploy'
#    end
#  end
#
#  desc 'Restart application'
#  task :restart do
#    on roles(:app), in: :sequence, wait: 5 do
#      invoke 'puma:restart'
#    end
#  end
#
#  before :starting,     :check_revision
#  after  :finishing,    :compile_assets
#  after  :finishing,    :cleanup
#  after  :finishing,    :restart
#end
