FactoryBot.define do
  factory :tournament do
    oka_first { 15 }
    oka_second { 5 }
    name { "Test" }
    total_rounds { 4 }
    time_limit { 80 }
    start_score { 25000 }
    user { create(:user) }

    before(:create) do |tournament|
      build_list(:player, 4)
    end
  end
end
