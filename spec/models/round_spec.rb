require 'rails_helper'

RSpec.describe Round, type: :model do
  let!(:tournament) { FactoryBot.create(:tournament, players: FactoryBot.build_list(:player, 4), oka_first: 20, oka_second: 10) }
  context "score_oka" do
    it "calculate correctly" do
      round = FactoryBot.create(:round, tournament: tournament, player_rounds: [ 
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[0], wind: 0, score: 25000),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[1], wind: 1, score: 25000),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[2], wind: 2, score: 25000),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[3], wind: 3, score: 25000)
        ])
	  round.score_oka

      list = tournament.player_rounds.order(oka: :desc)
	  expect(list[0].oka.to_d).to eq(35.to_d)
	  expect(list[1].oka.to_d).to eq(5.to_d)
	  expect(list[2].oka.to_d).to eq(-15.to_d)
	  expect(list[3].oka.to_d).to eq(-25.to_d)

	  expect(list[0].wind).to eq("east")
	  expect(list[1].wind).to eq("south")
	  expect(list[2].wind).to eq("west")
	  expect(list[3].wind).to eq("north")
	end

	it "calculate correctly with roudning" do
      round = FactoryBot.create(:round, tournament: tournament, player_rounds: [ 
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[0], wind: 0, score: 35700),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[1], wind: 1, score: 32400),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[2], wind: 2, score: 22200),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[3], wind: 3, score: 9700)
        ])
	  round.score_oka
      list = tournament.player_rounds.order(oka: :desc)
	  expect(list[0].oka.to_d).to eq(46.to_d)
	  expect(list[1].oka.to_d).to eq(12.to_d)
	  expect(list[2].oka.to_d).to eq(-18.to_d)
	  expect(list[3].oka.to_d).to eq(-40.to_d)
	end

    it "ties favor wind" do
      round = FactoryBot.create(:round, tournament: tournament, player_rounds: [ 
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[0], wind: 3, score: 25000),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[1], wind: 2, score: 25000),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[2], wind: 1, score: 25000),
        FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[3], wind: 0, score: 25000)
        ])
	  round.score_oka

      list = tournament.player_rounds.order(oka: :desc)
	  expect(list[0].oka.to_d).to eq(35.to_d)
	  expect(list[1].oka.to_d).to eq(5.to_d)
	  expect(list[2].oka.to_d).to eq(-15.to_d)
	  expect(list[3].oka.to_d).to eq(-25.to_d)

	  expect(list[0].wind).to eq("east")
	  expect(list[1].wind).to eq("south")
	  expect(list[2].wind).to eq("west")
	  expect(list[3].wind).to eq("north")
	end
  end
end


