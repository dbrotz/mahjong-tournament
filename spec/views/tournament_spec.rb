require "rails_helper"

describe "Tournament", type: :feature do
  context '#create' do
    before do
      visit new_tournament_path
    end

    it 'creates a new tournament' do
      find("#tournament_name").fill_in with: "Test"
      find("#participants").fill_in with: "Test1\r\nTest2\r\nTest3\r\nTest4\r\n"
      find("[name=commit]").click
      expect(Tournament.count).to be(1)
    end

    it 'Not enough players' do
      find("#tournament_name").fill_in with: "Test"
      find("#participants").fill_in with: "Test1\r\nTest2\r\nTest3\r\n"
      find("[name=commit]").click
      expect(Tournament.count).to be(0)
      expect(page).to have_css("#errors", count: 1)
    end
  end

  context '#countdown' do
    let!(:tournament) { FactoryBot.create(:tournament, players: FactoryBot.build_list(:player, 4)) }
	let!(:round) { FactoryBot.create(:round, tournament: tournament, player_rounds: [ 
	  FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[0], wind: 0),
	  FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[1], wind: 1),
	  FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[2], wind: 2),
	  FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[3], wind: 3)
	  ])}
    before do
      login_as(tournament.user)
      visit tournament_countdown_path(tournament.alpha_id)
    end

	it "Should start the clock" do
	  expect(tournament.countdown).to be(nil)
	  find("input[type=submit]").click
	  expect(tournament.reload.countdown).to_not be(nil)
	end
  end
end
