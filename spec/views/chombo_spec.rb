require "rails_helper"

describe "Chombo", type: :feature do
  let!(:tournament) { FactoryBot.create(:tournament, players: FactoryBot.build_list(:player, 4)) }

  context '#create' do
    before do
      login_as(tournament.user)
      visit new_tournament_chombo_path(tournament.alpha_id)
    end

    it "successfully creates a Chombo." do
	  find("#chombo_player_name").fill_in with: tournament.players.first.name
      find("[name=commit]").click
      expect(Chombo.count).to be(1)
	end
  end
end

