require "rails_helper"

describe "Round", type: :feature do
  let!(:tournament) { FactoryBot.create(:tournament, players: FactoryBot.build_list(:player, 4)) }
  context '#create' do
    before do
      login_as(tournament.user)
      visit new_tournament_round_path(tournament.alpha_id)
    end
    it "successfully creates a round." do
	  names = [tournament.players[0].name, tournament.players[1].name, tournament.players[2].name, tournament.players[3].name]
	  all("input[type=text]").each do |tb|
        tb.fill_in with: names.shift
	  end
     find("[name=commit]").click
     expect(Round.count).to be(1)
    end
  end

  context '#edit' do
	let!(:round) { FactoryBot.create(:round, tournament: tournament, player_rounds: [ 
	  FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[0], wind: 0),
	  FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[1], wind: 1),
	  FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[2], wind: 2),
	  FactoryBot.build(:player_round, tournament: tournament, player: tournament.players[3], wind: 3)
	  ])}
    before do
      login_as(tournament.user)
      visit edit_tournament_round_path(tournament.alpha_id, round)
    end
    it "Sucessfully edits a round." do
     find("[name=commit]").click
	 expect(PlayerRound.all[0].score).to_not be(nil)
	 expect(PlayerRound.all[1].score).to_not be(nil)
	 expect(PlayerRound.all[2].score).to_not be(nil)
	 expect(PlayerRound.all[3].score).to_not be(nil)
	end
 end
end

