class CreateChombo < ActiveRecord::Migration[5.2]
  def change
    create_table :chombos do |t|
      t.references :player, foreign_key: true
    end
  end
end
