class CreateRound < ActiveRecord::Migration[5.2]
  def change
    create_table :rounds do |t|
      t.integer :round_number, null: false
      t.integer :order, null: false
	  t.references :tournament, foreign_key: true
    end
  end
end
