class AddTimestampToTournaments < ActiveRecord::Migration[5.2]
  def change
    change_table :tournaments do |t|
      t.timestamps default: DateTime.now
	end
  end
end
