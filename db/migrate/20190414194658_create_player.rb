class CreatePlayer < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
	  t.references :tournament, foreign_key: true
	  t.string :name, null: false
      t.boolean :can_score, null: false, default: true
    end
  end
end
