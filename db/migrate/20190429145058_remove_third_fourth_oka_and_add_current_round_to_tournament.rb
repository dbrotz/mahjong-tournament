class RemoveThirdFourthOkaAndAddCurrentRoundToTournament < ActiveRecord::Migration[5.2]
  def change
    change_table :tournaments do |t|
      t.remove :oka_third
      t.remove :oka_fourth
      t.column :current_round, :integer
    end
  end
end
