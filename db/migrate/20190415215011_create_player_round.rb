class CreatePlayerRound < ActiveRecord::Migration[5.2]
  def change
    create_table :player_rounds do |t|
      t.references :tournament, foreign_key: true
      t.references :round, foreign_key: true
      t.references :player, foreign_key: true
      t.integer :wind, null: false
      t.integer :score
      t.decimal :oka, decimal: 1
    end
  end
end
