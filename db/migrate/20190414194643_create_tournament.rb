class CreateTournament < ActiveRecord::Migration[5.2]
  def change
    create_table :tournaments do |t|
	  t.string :alpha_id, length: 5, null: false
      t.string :name, null: false
      t.integer :total_rounds, null: false
      t.integer :time_limit, null: false
      t.integer :start_score, null: false
      t.integer :chombo_cost, null: false, default: 20000
      t.datetime :countdown
      t.string :time_zone, default: "UTC"

      t.text :schedule

      t.decimal :oka_first, null: false
      t.decimal :oka_second, null: false
      t.decimal :oka_third, null: false
      t.decimal :oka_fourth, null: false
    end
  end
end
