# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


user = User.new

t = Tournament.new(alpha_id: "aaaaa", name: "Test Tournament", total_rounds: 2, countdown: DateTime.now.utc, time_limit: 80, start_score: 30000, oka_first: 15, oka_second: 5, players: [
  Player.new(name: "test1"),
  Player.new(name: "test2"),
  Player.new(name: "test3"),
  Player.new(name: "test4"),
  Player.new(name: "test5"),
  Player.new(name: "test6"),
  Player.new(name: "test7"),
  Player.new(name: "test8"),
  Player.new(name: "test9"),
  Player.new(name: "test10"),
  Player.new(name: "test11"),
  Player.new(name: "test12"),
  Player.new(name: "test13"),
  Player.new(name: "test14"),
  Player.new(name: "test15"),
  Player.new(name: "test16")
], user: user)

user.email = "#{t.alpha_id}@test.com"
t.save!
user.save!

players = t.players

r = Round.create!(round_number: 0, order: 0, tournament_id: t.id)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[0], wind: 0, score: 40000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[1], wind: 1, score: 30000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[2], wind: 2, score: 10000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[3], wind: 3, score: 20000)
r.score_oka

r = Round.create!(round_number: 0, order: 1, tournament_id: t.id)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[4], wind: 0, score: 40000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[5], wind: 1, score: 30000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[6], wind: 2, score: 10000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[7], wind: 3, score: 20000)
r.score_oka

r = Round.create!(round_number: 1, order: 0, tournament_id: t.id)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[0], wind: 0, score: 40000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[1], wind: 1, score: 30000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[2], wind: 2, score: 10000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[3], wind: 3, score: 20000)
r.score_oka

r = Round.create!(round_number: 1, order: 1, tournament_id: t.id)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[4], wind: 0, score: 42000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[5], wind: 1, score: 28000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[6], wind: 2, score: 12000)
PlayerRound.create!(tournament_id: t.id, round_id: r.id, player: players[7], wind: 3, score: 18000)
r.score_oka
